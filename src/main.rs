const SLEEP_TIME  : u64   = 140;

extern crate conway_core;
extern crate conway_templates;

fn main() {
    let mut cw = conway_templates::new_toad();

    println!("{}", cw);
    loop {
        cw.next_step();
        println!("{}", cw);
        std::thread::sleep(std::time::Duration::from_millis(SLEEP_TIME));
    }
}
